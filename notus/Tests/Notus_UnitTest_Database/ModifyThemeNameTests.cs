﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class ModifyThemeNameTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
        }

        [Test]
        public void ModifyThemeName_ExistingTheme_ModifiesName()
        {
            List<string> listcolor = new List<string>() { "Blue", "Dark", "Grey" };
            Theme theme = new Theme("ocean", listcolor);
            database.GetThemeList().Add(theme);
            string newName = "Light";
            database.ModifyThemeName(theme, newName);
            Assert.That(theme.GetName(), Is.EqualTo(newName));
        }

        [Test]
        public void ModifyThemeName_NonExistingTheme_ThrowsException()
        {
            List<string> listcolor = new List<string>() { "Blue", "Dark", "Grey" };
            Theme theme = new Theme("ocean", listcolor);
            string newName = "Light";
            Assert.Throws<NotFoundException>(() => database.ModifyThemeName(theme, newName), "Theme not found.");
        }
    }
}
