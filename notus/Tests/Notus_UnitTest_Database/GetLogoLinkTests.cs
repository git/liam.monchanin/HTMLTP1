using Biblioteque_de_Class;

namespace Notus_UnitTest_Database
{
    [TestFixture]
    public class GetLogoLinksTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
            database.GetDefaultLogoList().Add(new Logo("Logo1", "link1"));
            database.GetDefaultLogoList().Add(new Logo("Logo2", "link2"));
            database.GetDefaultLogoList().Add(new Logo("Logo3", "link3"));
        }

        [Test]
        public void GetLogoLink_LogoExists_ReturnsLogoLink()
        {
            Assert.That(database.GetLogoLink("Logo2"), Is.EqualTo("link2"));
        }

        [Test]
        public void GetLogoLink_LogoDoesNotExist_ThrowsException()
        {
            string logoName = "Logo4";
            Assert.Throws<NotFoundException>(() => database.GetLogoLink(logoName));
        }
    }
}