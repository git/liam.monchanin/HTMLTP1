﻿using Biblioteque_de_Class;
using Notus_Persistance;
using System.Collections;
using System.Security.Cryptography;
using System.Text;

// load database
PersistenceManager manager = new(new Stub());
Database db = manager.LoadDatabaseData();


// initialization zone==============================================================================

bool continuerboucle = false;
bool menu = true, connection = false, inscription = false;
bool note=false, tags=false, para=false, paraCompte=false, theme=false;

// déclaration d'un user qui sera utiliser pour servir de personne connecté dans l'app
User u = new("", "", "");
User uvide = new("", "", "");
uvide.SetIsConnected(false);

// déclaration d'une note qui sera utiliser pour servir de note selectionnée
Note n = new("","",uvide);

List<Note> researchlist = new();

// factorisation 
bool continuer()
{
    continuerboucle = false;
    while(!continuerboucle)
    {
        Console.WriteLine("\nContinuer ? (O/n)");
        switch (Console.ReadLine())
        {
            case "O":
                return true;
            case null:
                return true;
            case "o":
                return true;
            case "n":
                return false;
            default:
                Console.WriteLine("\nEntrez un choix valide.\n");
                continuerboucle = true;
                break;
        }
    }
    return false;
}

bool choix_note()
{
    Console.WriteLine("\nChoisissez le nom de la note");
    string? wantedModifyNote = Console.ReadLine();
    wantedModifyNote??= "";
    try
    {
        n = u.GetNoteByName(wantedModifyNote);
    }catch (Exception ex) { Console.WriteLine(ex.Message); return false; }
    return true;
}

void displayNote()
{
    foreach (Note note in u.GetNoteList())
    {
        Console.WriteLine(note.GetName() + "\n");
    }
}

void displayTag()
{
    foreach (Tags tag in u.GetTagList())
    {
        Console.WriteLine(tag.GetName() + "\n");
    }
}

void displayTheme()
{
    foreach (Theme theme in db.GetThemeList())
    {
        Console.WriteLine(theme.GetName() + "\n");
    }
}

List<string>? choix_couleur()
{
    List<string> colorList = new();
    Console.WriteLine("Fond : ");
    string? wantedNewThemeFond = Console.ReadLine();
    if(wantedNewThemeFond == null){ return null; }
    else if(wantedNewThemeFond[0].ToString() != "#"){ 
        Console.WriteLine("\nLa couleur doit être au format hexadécimal.\n");
        return null;
    }else if(wantedNewThemeFond.Length != 7)
    {
        string toadd=""; 
        for(int i = 7- wantedNewThemeFond.Length; i!=0; i--){toadd += "0";}
        wantedNewThemeFond += toadd;
        return null;
    }
    Console.WriteLine("Texte : ");
    string? wantedNewThemeTexte = Console.ReadLine();
    if(wantedNewThemeTexte == null){ return null; }
    else if(wantedNewThemeTexte[0].ToString() != "#"){
        Console.WriteLine("\nLa couleur doit être au format hexadécimal.\n");
        return null;
    }else if(wantedNewThemeTexte.Length != 7)
    {
        string toadd=""; 
        for(int i = 7- wantedNewThemeTexte.Length; i!=0; i--){toadd += "0";}
        wantedNewThemeTexte += toadd;
        return null;
    }
    Console.WriteLine("Bouton : ");
    string? wantedNewThemeBouton = Console.ReadLine();
    if(wantedNewThemeBouton == null){ return null; }
    else if(wantedNewThemeBouton[0].ToString() != "#"){
        Console.WriteLine("\nLa couleur doit être au format hexadécimal.\n");
        return null;
    }else if(wantedNewThemeBouton.Length != 7)
    {
        string toadd=""; 
        for(int i = 7- wantedNewThemeBouton.Length; i!=0; i--){toadd += "0";}
        wantedNewThemeBouton += toadd;
        return null;
    }
    colorList.Add(wantedNewThemeFond);
    colorList.Add(wantedNewThemeTexte);
    colorList.Add(wantedNewThemeBouton);
    return colorList;
}

while (menu)
{
    Console.WriteLine("\n|--------------------------------------|");
    Console.WriteLine("|                                      |");
    Console.WriteLine("|          starting menu               |");
    Console.WriteLine("|                                      |");
    Console.WriteLine("|--------------------------------------|--------|");
    Console.WriteLine("|                                               |");
    Console.WriteLine("|  1 / - connection -                           |");
    Console.WriteLine("|  2 / - inscription -                          |");
    Console.WriteLine("|                                               |");
    Console.WriteLine("|-----------------------------------------------|\n");
    Console.WriteLine("rentrez votre choix.");
    switch (Console.ReadLine())
    {
        case "1": ///Connexion
            connection = true; break;

        case "2":///Creer un compte
            inscription = true; break;
        default:
            Console.WriteLine("\nEntrez un choix valide.\n");
            break;
    }

    //connection
    while (connection)
    {
        connection = false;
        Console.WriteLine("\nEntrez un nom : ");
        string? nom = Console.ReadLine();
        if (nom == null) { continue; }
        Console.WriteLine("\nEntrez un password :");
        string? password = Console.ReadLine();
        if (password == null) { continue; }
        try
        {
            u = db.GetUser(nom);
        }
        catch (AlreadyUsedException ex)
        {
            Console.WriteLine(ex.Message);
            connection = true;
        }
        if (!connection)
        {
            if (Database.ComparePassword(u, password.GetHashCode().ToString()))
            {
                u.SetIsConnected(true);
                Console.WriteLine("\nConnection réussie !\n");
                menu = false;
            }
            else
            {
                Console.WriteLine("\nWrong PassWord !\n");
                connection = true;
                continuerboucle = true;
                u = uvide;
            }
        }
        while (continuerboucle) { connection = continuer(); }
    }

    //inscription
    while (inscription)
    {
        Console.WriteLine("\nEntrez un nom :");
        string? nom = Console.ReadLine();
        if (nom == null) { continue; }
        Console.WriteLine("\nEntrez un password :");
        string? password = Console.ReadLine();
        if (password == null) { continue; }
        try
        {
            u = db.GetUser(nom);
        }
        catch (AlreadyUsedException)
        {
            u = new User(nom, "", password.GetHashCode().ToString());
            db.AddUser(u);
            db.GetUser(nom).SetIsConnected(true);
            Console.WriteLine("\nConnection réussie !\n");
            menu = false;
            break;
        }
        Console.WriteLine("\nNom d'utilisateur déjà utilisé. \n");
        while (continuerboucle) { inscription = continuer(); }

    }
}
//une fois connecté ou inscription fait
while (u.GetIsConnected())
{
    Console.WriteLine("\n|--------------------------------------|");
    Console.WriteLine("|                                      |");
    Console.WriteLine("|                menu                  |");
    Console.WriteLine("|                                      |");
    Console.WriteLine("|--------------------------------------|--------|");
    Console.WriteLine("|                                               |");
    Console.WriteLine("|  1/ - rechercher note -                       |");
    Console.WriteLine("|  2/ - note -                                  |");
    Console.WriteLine("|  3/ - tags -                                  |");
    Console.WriteLine("|  4/ - paramêtres -                            |");
    Console.WriteLine("|  5/ - se déconnecter -                        |");
    Console.WriteLine("|                                               |");
    Console.WriteLine("|-----------------------------------------------|\n");
    Console.WriteLine("rentrez votre choix.");
    switch (Console.ReadLine()) 
    {
        case "1":
            researchlist = u.GetNoteList();
            Console.WriteLine("\nEntrez la note que vous cherchez. ");
            string? wantedSearchNote = Console.ReadLine();
            Console.WriteLine("\nChercher par tags ? (o/N) ");
            
            Console.WriteLine("\nChercher par date ? (o/N) ");
            break;
        case "2":
            note = true;
            break;
        case "3":
            tags = true;
            break;
        case "4":
            para = true;
            break;
        case "5":
            Console.WriteLine("\ndéconnecté! \n");
            u.SetIsConnected(false);
            u = uvide;
            break;
        default: 
            Console.WriteLine("\nEntrez un choix valide.\n");
            break;
    }

    while(note)
    {
        Console.WriteLine("\n|--------------------------------------|");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|           menu - note                |");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|--------------------------------------|--------|");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|  1/ - afficher la liste des notes -           |");
        Console.WriteLine("|  2/ - afficher une note -                     |");
        Console.WriteLine("|  3/ - modifier une note -                     |");
        Console.WriteLine("|  4/ - écrire dans une note -                  |");
        Console.WriteLine("|  5/ - créer note -                            |");
        Console.WriteLine("|  6/ - supprimer note -                        |");
        Console.WriteLine("|  7/ - retour -                                |");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|-----------------------------------------------|\n");
        Console.WriteLine("note actuelle : " + n.GetName());
        Console.WriteLine("rentrez votre choix.");
        switch (Console.ReadLine()) 
        {
            case "1":
                displayNote();
                break;
            case "2":
                if (!choix_note()) { break; }
                Console.WriteLine("\n" + n.GetName() + " :");
                Console.WriteLine(n.GetText());
                break;
            case "3":
                if (!choix_note()) { break;}
                Console.WriteLine("\nChoisissez le nouveau nom de la note (entrer - nom par defaut)");
                string? wantedNewNameNote = Console.ReadLine();
                wantedNewNameNote??= "";
                n.SetName(wantedNewNameNote);
                break;
            case "4":
                if (!choix_note()) { break; }
                Console.WriteLine(n.GetText());
                Console.WriteLine("\nEntrez le texte à ajouter");
                string? wantedTextNote = Console.ReadLine();
                wantedTextNote??= "";
                n.AddText(wantedTextNote);
                break;
            case "5":
                Console.WriteLine("\nChoisissez le nom de la note (entrer - nom par defaut)");
                string? wantedNameNote = Console.ReadLine();
                wantedNameNote ??= "";
                u.CreateNote(wantedNameNote, "");
                break;
            case "6":
                Console.WriteLine("\nChoisissez le nom de la note");
                string? wantedDeleteNote = Console.ReadLine();
                wantedDeleteNote ??= "";
                try
                {
                    u.DeleteNote(wantedDeleteNote);
                }
                catch (Exception ex) { Console.WriteLine(ex.Message); }
                break;
            case "7":
                note = false;
                break;
            default: 
                Console.WriteLine("\nEntrez un choix valide.\n");
                break;
        }
    }

    while (tags) 
    {
        Console.WriteLine("\n|--------------------------------------|");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|           menu - tags                |");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|--------------------------------------|--------|");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|  1/ - créer tag -                             |");
        Console.WriteLine("|  2/ - ajouter tag -                           |");
        Console.WriteLine("|  3/ - supprimer tag -                         |");
        Console.WriteLine("|  4/ - retour -                                |");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|-----------------------------------------------|\n");
        Console.WriteLine("rentrez votre choix.");
        switch (Console.ReadLine()) 
        {
            case "1":
                Console.WriteLine("\nChoisissez le nom du tag.");
                string? wantedNameTag = Console.ReadLine();
                wantedNameTag??= "NoName" + u.GetTagList().Count.ToString();
                Console.WriteLine("\nChoisissez la couleur du tag.");
                string? wantedColorTag = Console.ReadLine();
                wantedColorTag??= "#000000";
                if (wantedColorTag[0] != '#') { wantedColorTag = "#" + wantedColorTag; }
                else if (wantedColorTag.Length < 7) { string toadd=""; for(int i = 7-wantedColorTag.Length; i!=0; i--){toadd += "0";} wantedColorTag += toadd ;}
                else if (wantedColorTag.Length > 7) { wantedColorTag = wantedColorTag[..7];}
                u.CreateTag(wantedNameTag,wantedColorTag);
                break;
            case "2":
                Note wantedAddNote;
                Tags wantedAddTag;
                Console.WriteLine("\n Plusieurs tags à ajouter ? (o/N)");
                string? wantedAddMultipleTag = Console.ReadLine();
                wantedAddMultipleTag ??= "N";
                if(wantedAddMultipleTag == "o" || wantedAddMultipleTag == "O"){
                    displayNote();
                    Console.WriteLine("\nDonnez le nom de la note à laquelle ajouter des tags : ");
                    string? wantedAddNameNote = Console.ReadLine();
                    wantedAddNameNote??= "";
                    try{
                        wantedAddNote = u.GetNoteByName(wantedAddNameNote);
                    }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                    displayTag();
                    Console.WriteLine("\nChoisissez les noms des tags séparés par des espaces.");
                    string? wantedAddNameTags = Console.ReadLine();
                    wantedAddNameTags??= "";
                    string[] wantedAddNameTagsList = wantedAddNameTags.Split(' ');
                    foreach(string wantedAddNameTag in wantedAddNameTagsList)
                    {
                        try
                        {
                            wantedAddTag = u.GetTagByName(wantedAddNameTag);
                        }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                        try
                        {
                            u.AddTagToNoteList(wantedAddNote, wantedAddTag);
                        }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                    }
                }else if(wantedAddMultipleTag == "n" || wantedAddMultipleTag == "N"){
                    displayNote();
                    Console.WriteLine("\nChoisissez le nom de la note.");
                    string? wantedAddNameNote = Console.ReadLine();
                    wantedAddNameNote??= "";
                    displayTag();
                    Console.WriteLine("\nChoisissez le nom du tag.");
                    string? wantedAddNameTag = Console.ReadLine();
                    wantedAddNameTag??= "";
                    try
                    {
                        wantedAddNote = u.GetNoteByName(wantedAddNameNote);
                        wantedAddTag = u.GetTagByName(wantedAddNameTag);
                        u.AddTagToNoteList(wantedAddNote, wantedAddTag);
                    }catch (Exception ex) { Console.WriteLine(ex.Message); }
                }else{
                    Console.WriteLine("\nEntrez un choix valide.\n");
                }
                break;
            case "3":
                displayNote();
                Note wantedRemoveNote;
                Tags wantedRemoveTag;
                Console.WriteLine("\n Choisissez le nom de la note à laquelle supprimer des tags : ");
                string? wantedRemoveNameNote = Console.ReadLine();
                wantedRemoveNameNote??= "";
                try{
                    wantedRemoveNote = u.GetNoteByName(wantedRemoveNameNote);
                }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                foreach( Tags t in u.GetNoteTaggedList(wantedRemoveNote))
                {
                    Console.WriteLine(t.GetName() + "\n");
                }
                Console.WriteLine("\nChoisissez le nom du tag à supprimer.");
                string? wantedRemoveNameTag = Console.ReadLine();
                wantedRemoveNameTag??= "";
                try {
                    wantedRemoveTag = u.GetTagByName(wantedRemoveNameTag);
                }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                try
                {
                    u.RemoveTagFromNoteList(wantedRemoveNote, wantedRemoveTag);
                }catch (Exception ex) { Console.WriteLine(ex.Message); }
                break;
            case "4":
                tags = false;
                break;
            default: 
                Console.WriteLine("\nEntrez un choix valide.\n");
                break;
        }
    }

    while(para)
    {
        Console.WriteLine("\n|--------------------------------------|");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|           menu - paramêtre           |");
        Console.WriteLine("|                                      |");
        Console.WriteLine("|--------------------------------------|--------|");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|  1/ - modifier compte -                       |");
        Console.WriteLine("|  2/ - thèmes -                                |");
        Console.WriteLine("|  3/ - supprimer un compte -                   |");
        Console.WriteLine("|  4/ - retour -                                |");
        Console.WriteLine("|                                               |");
        Console.WriteLine("|-----------------------------------------------|\n");
        Console.WriteLine("rentrez votre choix.");
        switch (Console.ReadLine()) 
        {
            case "1":
                paraCompte = true;
                break;
            case "2":
                theme = true;
                break;
            case "3":
                Console.WriteLine("\nÊtes-vous sûr de vouloir supprimer votre compte ? (o/N)");
                string? wantedDelete = Console.ReadLine();
                wantedDelete??= "N";
                if( wantedDelete == "o" || wantedDelete == "O"){
                    db.RemoveUser(u);
                    Console.WriteLine("\nVotre compte a bien été supprimé.\n");
                    para = false;
                    u = uvide;
                    break;
                }else if( wantedDelete == "n" || wantedDelete == "N"){
                    break;
                }
                break;
            case "4":
                para = false;
                break;
            default: 
                Console.WriteLine("\nEntrez un choix valide.\n");
                break;
        }
        while (paraCompte)
        {
            Console.WriteLine("\n|--------------------------------------|");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|           paramêtre - compte         |");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|--------------------------------------|--------|");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|  1/ - modifier pseudo -                       |");
            Console.WriteLine("|  2/ - modifier mot de passe -                 |");
            Console.WriteLine("|  3/ - retour -                                |");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|-----------------------------------------------|\n");
            Console.WriteLine("rentrez votre choix.");
            switch (Console.ReadLine()) 
            {
                case "1":
                    Console.WriteLine("\nChoisissez le nouveau pseudo.");
                    string? wantedNewPseudo = Console.ReadLine();
                    if(wantedNewPseudo == null){break;}
                    u.SetUsername(wantedNewPseudo);
                    break;
                case "2":
                    Console.WriteLine("\nChoisissez le nouveau mot de passe.");
                    string? wantedNewPassword = Console.ReadLine();
                    if(wantedNewPassword == null){break;}
                    else if(wantedNewPassword.Length < 8){
                        Console.WriteLine("\nLe mot de passe doit contenir au moins 8 caractères.\n");
                        break;
                    }
                    if(wantedNewPassword.GetHashCode().ToString() == u.GetPassword()){
                        Console.WriteLine("\nLe nouveau mot de passe doit être différent de l'ancien.\n");
                        break;
                    }
                    u.SetPassword(wantedNewPassword);
                    break;
                case "3":
                    paraCompte = false;
                    break;
                default: 
                    Console.WriteLine("\nEntrez un choix valide.\n");
                    break;
            }
        }
        while (theme)
        {
            Console.WriteLine("\n|--------------------------------------|");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|           paramêtre - thèmes         |");
            Console.WriteLine("|                                      |");
            Console.WriteLine("|--------------------------------------|--------|");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|  1/ - choisir un thème -                      |");
            Console.WriteLine("|  2/ - créer un thème -                        |");
            Console.WriteLine("|  3/ - supprimer un thème -                    |");
            Console.WriteLine("|  4/ - modifier un thème -                     |");
            Console.WriteLine("|  5/ - retour -                                |");
            Console.WriteLine("|                                               |");
            Console.WriteLine("|-----------------------------------------------|\n");
            Console.WriteLine("rentrez votre choix.");
            switch (Console.ReadLine()) 
            {
                case "1":
                    Theme twantedTheme;
                    displayTheme();
                    Console.WriteLine("\nChoisissez le nom du thème.");
                    string? wantedTheme = Console.ReadLine();
                    if(wantedTheme==null){break;}
                    try
                    {
                        twantedTheme = db.GetTheme(wantedTheme);
                    }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                    u.SetTheme(twantedTheme);
                    break;
                case "2":
                    List<string>? themeList;
                    Console.WriteLine("\nChoisissez le nom du thème.");
                    string? wantedNewThemeName = Console.ReadLine();
                    if(wantedNewThemeName==null){break;}
                    Console.WriteLine("\nChoisissez la couleur du thème.");
                    themeList = choix_couleur();
                    if(themeList == null) { break;}
                    db.AddTheme(new Theme(wantedNewThemeName, themeList));
                    break;
                case "3":
                    Theme t;
                    Console.WriteLine("\nChoisissez le nom du thème à supprimer.");
                    string? wantedRemoveTheme = Console.ReadLine();
                    if(wantedRemoveTheme == null) {break;}
                    try{
                        t = db.GetTheme(wantedRemoveTheme);
                    }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                    try{
                        db.RemoveTheme(t);
                    }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                    break;
                case "4":
                    displayTheme();
                    Theme themeToModify;
                    Console.WriteLine("\nChoisissez le nom du thème à modifier.");
                    string? wantedModifyTheme = Console.ReadLine();
                    if(wantedModifyTheme == null){break;}
                    try{
                        themeToModify = db.GetTheme(wantedModifyTheme);
                    }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                    Console.WriteLine("\nChoisissez le nouveau nom du thème.");
                    string? wantedNewNameTheme = Console.ReadLine();
                    if(wantedNewNameTheme == null) {break;}
                    try{
                        db.ModifyThemeName(themeToModify, wantedNewNameTheme);
                    }catch (Exception ex) { Console.WriteLine(ex.Message); break;}
                    Console.WriteLine("\nChoisissez les nouvelles couleurs du thème.");
                    List<string>? couleurlist = choix_couleur();
                    if(couleurlist == null) { break;}
                    db.ModifyThemeColorList(themeToModify, couleurlist);
                    break;
                case "5":
                    theme = false;
                    break;
                default: 
                    Console.WriteLine("\nEntrez un choix valide.\n");
                    break;
            }
        }
    }
}