﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class GetThemeTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
        }

        [Test]
        public void GetTheme_ExistingTheme_ReturnsTheme()
        {
            List<string> listcolor = new List<string>() { "Blue", "Dark", "Grey" };
            Theme theme = new Theme("ocean", listcolor);
            database.GetThemeList().Add(theme);
            Theme retrievedTheme = database.GetTheme("ocean");
            Assert.That(retrievedTheme, Is.EqualTo(theme));
        }

        [Test]
        public void GetTheme_NonExistingTheme_ThrowsException()
        {
            Assert.Throws<NotFoundException>(() => database.GetTheme("NonExistingTheme"), "No theme found with this name.");
        }
    }
}
