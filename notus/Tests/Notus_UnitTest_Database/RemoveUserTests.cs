﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class RemoveUserTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
        }

        [Test]
        public void RemoveUser_ExistingUser_UserRemovedFromList()
        {
            User user = new User("John","rien","choco");
            user.SetEmail("john@example.com");
            database.GetUserList().Add(user);
            database.RemoveUser(user);
            CollectionAssert.DoesNotContain(database.GetUserList(), user);
        }

        [Test]
        public void RemoveUser_NonExistingUser_ThrowsException()
        {
            User user = new User("John", "rien", "choco");
            user.SetEmail("john@example.com");
            Assert.Throws<NotFoundException>(() => database.RemoveUser(user), "User not found.");
        }
    }
}
