﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class RemoveThemeTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
        }

        [Test]
        public void RemoveTheme_NewTheme_ThemeAddedToList()
        {
            List<string> listcolor = new List<string>() { "Blue", "Dark", "Grey" };
            Theme theme = new Theme("ocean", listcolor);
            database.AddTheme(theme);
            database.RemoveTheme(theme);
            CollectionAssert.DoesNotContain(database.GetThemeList(), theme);
        }

        [Test]
        public void RemoveTheme_ExistingTheme_ThrowsException()
        {
            List<string> listcolor = new();
            Theme theme = new Theme("ocean", listcolor);
            Assert.Throws<NotFoundException>(() => database.RemoveTheme(theme), "Theme already used.");
        }
    }
}
