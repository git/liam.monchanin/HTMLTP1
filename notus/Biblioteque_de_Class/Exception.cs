﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    [Serializable]
    public class NotAllowedException : Exception
    {
        public NotAllowedException(string message) : base(message)
        {
        }
        protected  NotAllowedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    [Serializable]
    public class AlreadyUsedException : Exception
    {
        public AlreadyUsedException(string message) : base(message)
        {
        }
        protected  AlreadyUsedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    [Serializable]
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {
        }
        protected  NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    [Serializable]
    public class AlreadyExistException : Exception
    {
        public AlreadyExistException(string message) : base(message)
        {
        }
        protected  AlreadyExistException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }




    [Serializable]
    public class FileException : Exception
    {
        public FileException(string message) : base(message)
        {
        }
        protected  FileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
