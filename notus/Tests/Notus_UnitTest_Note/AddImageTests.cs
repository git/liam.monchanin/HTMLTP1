﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Note
{
    public class AddImageTests
    {
        [Test]
        public void AddImage_WhenImageLinkIsValid_ImageAddedToList()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            string imageLink = "image.png";
            string position = "top";
            note.AddImage(imageLink, position);
            Assert.That(note.GetImageList().Count, Is.EqualTo(1));
        }
    }
}
