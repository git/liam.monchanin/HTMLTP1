﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    public class Logo
    {
        private string Name { get; set; }
        private string LogoLink { get; set; }

        public Logo(string name, string logoLink)
        {
            Name = name;
            LogoLink = logoLink;
        }

        public string GetName() { return Name; }
        public string GetLogoLink() { return LogoLink; }

        public void SetName(string name) { Name = name; }
        public void SetLogoLink(string logoLink) { LogoLink = logoLink; }

        public override string ToString() => $"Logo -> Name: {Name}\nLink: {LogoLink}";
    }
}
