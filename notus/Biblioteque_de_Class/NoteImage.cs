﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    public class NoteImage
    {
        private int Name { get; set; }
        private string ImageLink { get; set; }
        private string Position { get; set; }

        public NoteImage(int name, string imageLink, string position)
        {
            Name = name;
            ImageLink = imageLink;
            Position = position;
        }

        public int GetName() { return Name; }
        public string GetImageLink() { return ImageLink; }
        public string GetPosition() { return Position; }

        public void SetName(int name) { Name = name; }
        public void SetImageLink(string imageLink) { ImageLink = imageLink; }
        public void SetPosition(string position) { Position = position; }

        public override string ToString() => $"image -> name: {Name}\nlink: {ImageLink}";
    }
}
