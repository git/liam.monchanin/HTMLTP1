using Microsoft.Maui.Controls;
using Biblioteque_de_Class;
using Notus_Persistance;

namespace notus;

public partial class RecherPage : ContentPage
{
    public PersistenceManager manager { get; private set; } = new PersistenceManager(new Stub());
    public RecherPage()
	{
        manager.LoadDatabaseData();
		InitializeComponent();
        ListNote.BindingContext = manager;
	}

    void Profil_Clicked(System.Object sender, System.EventArgs e)
    {

    }

}