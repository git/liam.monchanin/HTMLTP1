﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    [DataContract(IsReference = true)]
    public class Database
    {
        [DataMember]
        private List<Logo> DefaultLogoList;
        [DataMember]
        private List<Theme> ThemeList;
        [DataMember]
        private List<User> UserList;
        [DataMember]
        private Dictionary<User, List<Theme>> AddedThemeList;

        public Database()
        {
            DefaultLogoList = new List<Logo>();
            ThemeList = new List<Theme>();
            UserList = new List<User>();
            AddedThemeList = new Dictionary<User, List<Theme>>();
        }

        public List<Logo> GetDefaultLogoList() { return DefaultLogoList; }
        public List<Theme> GetThemeList() { return ThemeList; }
        public List<User> GetUserList() { return UserList; }
        public Dictionary<User,List<Theme>> GetAddedThemeFromUser() { return AddedThemeList; }

        public void SetDefaultLogoList(List<Logo> defaultLogoList) {  DefaultLogoList = defaultLogoList; }
        public void SetDefaultThemeList(List<Theme> defaultThemeList) { ThemeList = defaultThemeList; }

        /// <summary>
        /// recherche un utilisateur dans la liste d'utilisateur
        /// </summary>
        public List<User> SearchUser(string name)
        {
            List<User> searchedUsers = new List<User>();
            string search = name.ToLower();
            searchedUsers.AddRange(UserList.Where( user => user.GetUsername().ToLower().Contains(search)));
            return searchedUsers;
        }

        /// <summary>
        /// récupérer le lien d'un logo
        /// </summary>
        public string? GetLogoLink(string name)
        {
            foreach (Logo logo in DefaultLogoList)
            {
                if (logo.GetName() == name) { return logo.GetLogoLink(); }
            }
            throw new NotFoundException("No logo link found.");
        }

        /// <summary>
        /// récupérer un utilisateur
        /// </summary>
        public User GetUser(string name)
        {
            foreach (User user in UserList)
            {
                if (user.GetUsername() == name)
                {
                    return user;
                }
            }
            throw new AlreadyUsedException("No user found with this username.");
        }

        /// <summary>
        /// comparer le mot de passe entré avec celui de l'utilisateur
        /// </summary>
        public static bool ComparePassword(User user, string? password) => user.GetPassword() == password;

        /// <summary>
        /// rechercher un mail dans la liste d'utilisateur
        /// </summary>
        public bool FindEmail(string email)
        {
            foreach (User user in UserList)
            {
                if (string.Equals(user.GetEmail(), email))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// ajouter un utilisateur dans la liste d'utilisateur
        /// </summary>
        public void AddUser(User user)
        {
            foreach (User existingUser in UserList)
            {
                if (existingUser.GetUsername() == user.GetUsername())
                {
                    throw new AlreadyUsedException("Username already used.");
                }
                else if (existingUser.GetEmail() == user.GetEmail())
                {
                    throw new AlreadyUsedException("Email already used.");
                }
            }
            UserList.Add(user);
        }

        /// <summary>
        /// supprimer un utilisateur dans la liste d'utilisateur
        /// </summary>
        public void RemoveUser(User user)
        {
            if (UserList.Contains(user))
            {
                UserList.Remove(user);
            }
            else
            {
                throw new NotFoundException("User not found.");
            }
        }

        /// <summary>
        /// ajouter un theme dans la liste de theme
        /// </summary>
        public void AddTheme(Theme theme)
        {
            foreach (Theme existingTheme in ThemeList)
            {
                if (existingTheme.GetName() == theme.GetName())
                {
                    throw new AlreadyUsedException("Theme already used.");
                }
            }
            ThemeList.Add(theme);
        }

        /// <summary>
        /// supprimer un theme dans la liste de theme
        /// </summary>
        public void RemoveTheme(Theme theme)
        {
            if (ThemeList.Contains(theme)) 
            {
                if (theme.GetName().Length > 6 && theme.GetName()[..6] != "Static" )
                {
                    throw new AlreadyUsedException("This theme is used a default theme.");
                }
                ThemeList.Remove(theme);
            }
            else
            {
                throw new NotFoundException("Theme not found.");
            }
        }

        /// <summary>
        /// récupérer un theme
        /// </summary>
        public Theme GetTheme(string name)
        {
            foreach (Theme theme in ThemeList)
            {
                if (theme.GetName() == name)
                {
                    return theme;
                }
            }
            throw new NotFoundException("No theme found with this name.");
        }

        /// <summary>
        /// modifier le nom d'un theme
        /// </summary>
        public void ModifyThemeName(Theme theme, string newName)
        {
            foreach (Theme existingTheme in ThemeList)
            {
                if (existingTheme.GetName() == theme.GetName())
                {
                    existingTheme.SetName(newName);
                    return;
                }
            }
            throw new NotFoundException("Theme not found.");
        }

        /// <summary>
        /// modifier la liste de couleur d'un theme
        /// </summary>
        public void ModifyThemeColorList(Theme theme, List<string> newColorList)
        {
            foreach (Theme existingTheme in ThemeList)
            {
                if (existingTheme.GetName() == theme.GetName())
                {
                    for (int i = 0; i < 3; i++)
                    {
                        existingTheme.ChangeColor(existingTheme.GetColor(i), newColorList[i]);
                    }
                    return;
                }
            }
        }

        public List<Theme> AddedThemeOfOneUser(User user)
        {
            return GetAddedThemeFromUser()[user];
        }
    }
}