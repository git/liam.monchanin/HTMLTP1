﻿using Biblioteque_de_Class;

namespace Notus_UnitTest_Database
{
    [TestFixture]
    public class SearchUserTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
            database.GetUserList().Add(new User("John", "john@example.com", "choco"));
            database.GetUserList().Add(new User("Jane", "jane@example.com", "choco"));
            database.GetUserList().Add(new User("Alice", "alice@example.com", "choco"));
        }

        [Test]
        public void SearchUser_UserExists_ReturnsMatchingUsers()
        {
            string searchName = "Jo";
            List<User> searchedUsers = database.SearchUser(searchName);
            Assert.That(searchedUsers.Count, Is.EqualTo(1));
            Assert.That(searchedUsers[0].GetUsername(), Is.EqualTo("John"));
        }

        [Test]
        public void SearchUser_UserDoesNotExist_ReturnsEmptyList()
        {
            string searchName = "Bob";
            List<User> searchedUsers = database.SearchUser(searchName);
            Assert.IsEmpty(searchedUsers);
        }

        [Test]
        public void SearchUser_CaseInsensitiveSearch_ReturnsMatchingUsers()
        {
            string searchName = "ALICE";
            List<User> searchedUsers = database.SearchUser(searchName);
            Assert.That(searchedUsers.Count, Is.EqualTo(1));
            Assert.That(searchedUsers[0].GetUsername(), Is.EqualTo("Alice"));
        }

        [Test]
        public void SearchUser_PartialMatch_ReturnsMatchingUsers()
        {
            string searchName = "ane";
            List<User> searchedUsers = database.SearchUser(searchName);
            Assert.That(searchedUsers.Count, Is.EqualTo(1));
            Assert.That(searchedUsers[0].GetUsername(), Is.EqualTo("Jane"));
        }
    }
}