﻿using Microsoft.VisualBasic;

namespace Biblioteque_de_Class
{
    public class PersistenceManager
    {
        private Database db = new();

        private readonly IManager persistence;

        public PersistenceManager(IManager pers)
        {
            persistence = pers; 
        }

        public void SaveDatabaseData(Database database)
        {
            persistence.SaveDatabaseData(database.GetUserList(), database.GetAddedThemeFromUser());
        }

        public Database LoadDatabaseData()
        {
            db = persistence.LoadDatabaseData();
            db.SetDefaultThemeList(persistence.LoadDefaultTheme());
            db.SetDefaultLogoList(persistence.LoadDefaultLogo());
            return db;
        }
    }
}
