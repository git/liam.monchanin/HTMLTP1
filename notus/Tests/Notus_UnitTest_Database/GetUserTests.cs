﻿using Biblioteque_de_Class;

namespace Notus_UnitTest_Database
{

    [TestFixture]
    public class GetUserTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
            database.GetUserList().Add(new User("John","rien","choco"));
            database.GetUserList().Add(new User("Alice", "rien", "choco"));
            database.GetUserList().Add(new User("Bob", "rien", "choco"));
        }

        [Test]
        public void GetUser_UserExists_ReturnsUser()
        {
            string userName = "Alice";
            User user = database.GetUser(userName);
            Assert.IsNotNull(user);
            Assert.That(user.GetUsername(), Is.EqualTo(userName));
        }

        [Test]
        public void GetUser_UserDoesNotExist_ThrowsException()
        {
            string userName = "Eve";
            Assert.Throws<AlreadyUsedException>(() => database.GetUser(userName));
        }
    }
}
