using Biblioteque_de_Class;

namespace Notus_UnitTest_User
{
    [TestFixture]
    public class SearchNoteByNameTests
    {
        private User owner;
        private string searchName;
        
        [SetUp]
        public void SetUp()
        {
            owner = new("Owner", "owner@example.com", "password");
            owner.CreateNote("Note 1", "image1.png");
            owner.CreateNote("Note 2", "image2.png");
            owner.CreateNote("Another Note", "image3.png");
            owner.AddFavorite(owner.GetNoteList()[0]);
            owner.AddFavorite(owner.GetNoteList()[1]);
            owner.AddFavorite(owner.GetNoteList()[2]);
            searchName = "note";
        }

        [Test]
        public void SearchNoteByName_WhenMatchingNotesExist_NotesReturned()
        {
            List<Note> result = owner.SearchNoteByName(owner.GetNoteList(),"note");
            Assert.That(result, Has.Count.EqualTo(3));
            CollectionAssert.Contains(result, owner.GetNoteList()[0]);
            CollectionAssert.Contains(result, owner.GetNoteList()[1]);
            CollectionAssert.Contains(result, owner.GetNoteList()[2]);
        }

        [Test]
        public void SearchNoteByName_WhenNoMatchingNotesExist_EmptyListReturned()
        {
            List<Note> result = owner.SearchNoteByName(owner.GetNoteList(), "test");
            Assert.That(result, Is.Empty);
        }

        [Test]
        public void SearchFavoriteNoteByName_ShouldReturnMatchingNotes()
        {
            List<Note> searchedNotes = owner.SearchNoteByName(owner.GetFavList(), searchName);
            Assert.That(searchedNotes, Has.Count.EqualTo(3));
            CollectionAssert.Contains(searchedNotes, owner.GetNoteList()[0]);
            CollectionAssert.Contains(searchedNotes, owner.GetNoteList()[1]);
            CollectionAssert.Contains(searchedNotes, owner.GetNoteList()[2]);
        }

        [Test]
        public void SearchFavoriteNoteByName_ShouldReturnEmptyList_WhenNoMatchFound()
        {
            searchName = "nonexistent";
            List<Note> searchedNotes = owner.SearchNoteByName(owner.GetFavList(), searchName);
            Assert.That(searchedNotes, Is.Empty);
        }
    }
}