﻿using Biblioteque_de_Class;

namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class AddCollaboratorTests
    {
        [Test]
        public void AddCollaborator_WhenUserIsOwner_CollaboratorAdded()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User collaborator = new User("Collaborator1", "collaborator1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            note.AddCollaborator(owner, collaborator);
            Assert.Contains(collaborator, note.GetCollaborators());
        }

        [Test]
        public void AddCollaborator_WhenUserIsNotOwner_ThrowsException()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User collaborator = new User("Collaborator1", "collaborator1@example.com", "password");
            User user = new User("User1", "user1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            Assert.Throws<NotAllowedException>(() => note.AddCollaborator(user, collaborator));
        }
    }
}
