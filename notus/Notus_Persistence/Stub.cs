﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;


namespace Notus_Persistance
{
    public class Stub : IManager
    {
        public void SaveDatabaseData(List<User> UserList, Dictionary<User, List<Theme>> AddedThemeFromUser)
        {
            throw new NotImplementedException();
        }

        //Loaders
        Database IManager.LoadDatabaseData()
        {
            Database database = new Database();
            Note nselect;
            User uselect;

            // add some users
            database.AddUser(new User("Nicolas", "leHeros@gmail.com", "Feur"));
            database.AddUser(new User("Benjamin", "labsent@gmail.com", "Moto2005"));
            database.AddUser(new User("Liam", "liammonchanin@gmail.com", "Baguette"));
            database.AddUser(new User("Brigitte", "Macroutte@gmail.com", "49Trois"));

            // add some notes and tags to go faster
            foreach(User user in database.GetUserList().Where(x => x.GetUsername().Contains("m")))
            {
                user.CreateNote("Note 1", "");
                user.CreateNote("Note 2", "");
                user.CreateNote("Note 3", "");
                user.CreateTag("Tag 1","#FA0034");
                user.CreateTag("Tag 2", "#2500A4");
            }

            // add note to user for sharing note test mixed with tag
            uselect = (User)database.GetUserList().Where(x => x.GetUsername() == "Nicolas");
            uselect.CreateNote("Note 4", "Logo_1");
            uselect.CreateTag("Tag 3", "#00FF00");
            nselect = (Note)uselect.GetNoteList().Where(x => x.GetName() == "Note 4");
            uselect.AddTagToNoteList(nselect, (Tags)uselect.GetTagList().Where(x => x.GetName() == "Tag 3"));
            nselect.AddCollaborator(uselect,(User)database.GetUserList().Where(x => x.GetUsername() == "Benjamin"));
            uselect = (User)database.GetUserList().Where(x => x.GetUsername() == "Benjamin");
            uselect.CreateTag("Tag 4", "#FF0000");

            // add some default logos and themes
            database.GetDefaultLogoList().Add(new Logo("Logo_1", "logo"));
            database.GetDefaultLogoList().Add(new Logo("Logo_2", "logo"));
            database.GetDefaultLogoList().Add(new Logo("Logo_3", "logo"));
            List<string> colorListHexaCode = new("FF0000,00FF00,0000FF".Split(','));
            database.AddTheme(new Theme("Theme_1", colorListHexaCode));
            colorListHexaCode = new("FF00FF,00FFFF,FFFF00".Split(','));
            database.AddTheme(new Theme("Theme_2", colorListHexaCode));
            colorListHexaCode = new("000000,FFFFFF,000000".Split(','));
            database.AddTheme(new Theme("Theme_3", colorListHexaCode));

            foreach (User user in database.GetUserList())
            {
                user.SetPassword(user.GetPassword().GetHashCode().ToString());
            }

            return database;
        }

        public List<Theme> LoadDefaultTheme()
        {
            throw new NotImplementedException();
        }
        public List<Logo> LoadDefaultLogo()
        {
            throw new NotImplementedException();
        }

    }
}

