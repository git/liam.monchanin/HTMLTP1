﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class FindMailTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
            User user = new User("John","rien","choco");
            user.SetEmail("john@example.com");
            database.GetUserList().Add(user);
        }

        [Test]
        public void FindEmail_ExistingEmail_ReturnsTrue()
        {
            string email = "john@example.com";
            bool result = database.FindEmail(email);
            Assert.IsTrue(result);
        }

        [Test]
        public void FindEmail_NonExistingEmail_ReturnsFalse()
        {
            string email = "jane@example.com";
            bool result = database.FindEmail(email);
            Assert.IsFalse(result);
        }
    }
}
