﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class RemoveImageTests
    {
        [Test]
        public void RemoveImage_WhenImageExists_ImageRemovedFromList()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            note.AddImage("image.png", "top");
            note.RemoveImage(1);
            Assert.That(note.GetImageList().Count, Is.EqualTo(0));
        }

        [Test]
        public void RemoveImage_WhenImageDoesNotExist_ExceptionThrown()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            Assert.Throws<NotFoundException>(() => note.RemoveImage(1));
        }
    }
}
