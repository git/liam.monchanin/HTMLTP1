using Biblioteque_de_Class;
namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class VerifyOwnerTests
    {
        [Test]
        public void VerifyOwner_UserIsOwner_ReturnsTrue()
        {
            User owner = new User("John", "john@example.com", "choco");
            Note note = new Note("My Note", "path/to/logo.png", owner);
            bool isOwner = note.VerifyOwner(owner);
            Assert.IsTrue(isOwner);
        }

        [Test]
        public void VerifyOwner_UserIsNotOwner_ReturnsFalse()
        {
            User owner = new User("John", "john@example.com", "choco");
            User anotherUser = new User("Jane", "jane@example.com", "choco");
            Note note = new Note("My Note", "path/to/logo.png", owner);
            bool isOwner = note.VerifyOwner(anotherUser);
            Assert.IsFalse(isOwner);
        }
    }
}