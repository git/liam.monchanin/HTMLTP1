﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class SetTests
    {
        private User owner;
        private Note note;

        [SetUp]
        public void Setup()
        {
            owner = new User("John","rien","choco");
            note = new Note("note","logo.png",owner);
        }

        [Test]
        public void GetName_ShouldReturnName()
        {
            string actualName = note.GetName();
            Assert.That(actualName, Is.EqualTo("note"));
        }

        [Test]
        public void GetLogoPath_ShouldReturnLogoPath()
        {
            string actualLogoPath = note.GetLogoPath();
            Assert.That(actualLogoPath, Is.EqualTo("logo.png"));
        }

        [Test]
        public void GetCreationDate_ShouldReturnCreationDate()
        {
            DateOnly actualCreationDate = note.GetCreationDate();
            Assert.That(actualCreationDate, Is.EqualTo(DateOnly.FromDateTime(DateTime.Now)));
        }

        [Test]
        public void GetModificationDate_ShouldReturnModificationDate()
        {
            DateOnly actualModificationDate = note.GetModificationDate();
            Assert.That(actualModificationDate, Is.EqualTo(DateOnly.FromDateTime(DateTime.Now)));
        }

        [Test]
        public void GetImageList_ShouldReturnImageList()
        {
            List<NoteImage> actualImageList = note.GetImageList();
            Assert.That(actualImageList, Is.EqualTo(new List<NoteImage>()));
        }

        [Test]
        public void GetCollaborators_ShouldReturnCollaborators()
        {
            List<User> actualCollaborators = note.GetCollaborators();
            Assert.That(actualCollaborators, Is.EqualTo(new List<User>()));
        }

        [Test]
        public void GetEditors_ShouldReturnEditors()
        {
            List<User> actualEditors = note.GetEditors();
            Assert.That(actualEditors, Is.EqualTo(new List<User>()));
        }

        [Test]
        public void GetOwner_ShouldReturnOwner()
        {
            User actualOwner = note.GetOwner();
            Assert.That(actualOwner, Is.EqualTo(owner));
        }

    }
}
