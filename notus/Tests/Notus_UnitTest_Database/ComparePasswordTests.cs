﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    [TestFixture]
    public class ComparePasswordTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
            User user = new User("John","rien","choco");
            user.SetPassword("password123");
            database.GetUserList().Add(user);
        }

        [Test]
        public void ComparePassword_CorrectPassword_ReturnsTrue()
        {
            User user = database.GetUserList()[0];
            string password = "password123";
            var result = Database.ComparePassword(user, password);
            Assert.That(result, Is.True);
        }

        [Test]
        public void ComparePassword_IncorrectPassword_ReturnsFalse()
        {
            User user = database.GetUserList()[0];
            string password = "incorrectPassword";
            var result = Database.ComparePassword(user, password);
            Assert.That(result, Is.False);
        }
    }
}
