﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class RemoveCollaboratorTests
    {
        [Test]
        public void RemoveCollaborator_WhenUserIsOwner_CollaboratorRemoved()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User collaborator = new User("Collaborator1", "collaborator1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            note.AddCollaborator(owner, collaborator);
            note.RemoveCollaborator(owner, collaborator);
            Assert.IsFalse(note.GetCollaborators().Contains(collaborator));
        }

        [Test]
        public void RemoveCollaborator_WhenUserIsNotOwner_ThrowsException()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User collaborator = new User("Collaborator1", "collaborator1@example.com", "password");
            User user = new User("User1", "user1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            note.AddCollaborator(owner, collaborator);
            Assert.Throws<NotAllowedException>(() => note.RemoveCollaborator(user, collaborator));
        }
        
    }
}
