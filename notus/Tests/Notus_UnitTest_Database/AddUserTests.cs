﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class AddUserTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
        }

        [Test]
        public void AddUser_ValidUser_UserAddedToList()
        {
            User user = new User("John", "rien", "choco") ;
            user.SetEmail("john@example.com");
            database.AddUser(user);
            CollectionAssert.Contains(database.GetUserList(), user);
        }

        [Test]
        public void AddUser_DuplicateUsername_ThrowsException()
        {
            User existingUser = new User("John1", "rien", "choco");
            existingUser.SetEmail("john1@example.com");
            database.GetUserList().Add(existingUser);
            User newUser = new User("John1", "rien", "choco");
            newUser.SetEmail("Jane@example.com");
            Assert.Throws<AlreadyUsedException>(() => database.AddUser(newUser), "Username already used.");
        }

        [Test]
        public void AddUser_DuplicateEmail_ThrowsException()
        {
            User existingUser = new User("John2", "rien", "choco");
            existingUser.SetEmail("john2@example.com");
            database.GetUserList().Add(existingUser);
            User newUser = new User("Jane", "rien", "choco");
            newUser.SetEmail("john2@example.com");
            Assert.Throws<AlreadyUsedException>(() => database.AddUser(newUser), "Email already used.");
        }
    }
}
