﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class AddThemeTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
        }

        [Test]
        public void AddTheme_NewTheme_ThemeAddedToList()
        {
            List<string> listcolor = new List<string>() { "Blue", "Dark", "Grey" };
            Theme theme = new Theme("ocean", listcolor);
            database.AddTheme(theme);
            CollectionAssert.Contains(database.GetThemeList(), theme);
        }

        [Test]
        public void AddTheme_ExistingTheme_ThrowsException()
        {
            List<string> listcolor = new();
            Theme theme = new Theme("ocean", listcolor);
            database.AddTheme(theme);
            Assert.Throws<AlreadyUsedException>(() => database.AddTheme(theme), "Theme already used.");
        }
    }
}
