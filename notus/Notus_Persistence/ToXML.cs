﻿using Biblioteque_de_Class;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Notus_Persistance
{
    public class ToXML : IManager
    {
        private const string DataFilePath = "data.xml";
        private const string DefaultThemePath = "";
        private const string DefaultLogoPath = "";
        private static readonly DataContractSerializer DatabaseXmlSerializer = new(typeof(Database));

        public void SaveDatabaseData(List<User> UserList, Dictionary<User, List<Theme>> AddedThemeFromUser)
        {
            XmlWriterSettings settings = new() { Indent = true };
            using TextWriter tw = File.CreateText(DataFilePath);
            using XmlWriter writer = XmlWriter.Create(tw, settings);
            DatabaseXmlSerializer.WriteObject(writer, UserList);
            DatabaseXmlSerializer.WriteObject(writer, AddedThemeFromUser);
        }

        public Database LoadDatabaseData()
        {
            if (File.Exists(DataFilePath))
            {
                using (FileStream fileStream = File.OpenRead(DataFilePath))
                {
                    return DatabaseXmlSerializer.ReadObject(fileStream) is not Database database
                        ? throw new FileException("Failed to load the database. The loaded object is null.")
                        : database;
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }

        public List<Theme> LoadDefaultTheme()
        {
            if (File.Exists(DefaultThemePath))
            {
                using (FileStream fileStream = File.OpenRead(DefaultThemePath))
                {
                    return DatabaseXmlSerializer.ReadObject(fileStream) is not List<Theme> DefaultThemeList
                        ? throw new FileException("Failed to load Default Theme. The loaded object is null.")
                        : DefaultThemeList;
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }

        public List<Logo> LoadDefaultLogo()
        {
            if (File.Exists(DefaultLogoPath))
            {
                using (FileStream fileStream = File.OpenRead(DefaultLogoPath))
                {
                    return DatabaseXmlSerializer.ReadObject(fileStream) is not List<Logo> DefaultLogoList
                        ? throw new FileException("Failed to load Default Logo. The loaded object is null.")
                        : DefaultLogoList;
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }

    }
}
