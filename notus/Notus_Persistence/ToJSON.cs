﻿using Biblioteque_de_Class;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Notus_Persistance
{
    public class ToJSON : IManager
    {
        private const string DatabaseDataFilePath = "data.json";
        private const string DefaultThemePath = "";
        private const string DefaultLogoPath = "";
        private static readonly DataContractJsonSerializer DatabasejsonSerializer = new(typeof(Database));

        public void SaveDatabaseData(List<User> UserList, Dictionary<User, List<Theme>> AddedThemeFromUser)
        {
            using (FileStream fileStream = File.Create(DatabaseDataFilePath))
            {
                using (var writer = JsonReaderWriterFactory.CreateJsonWriter(
                            fileStream,
                            System.Text.Encoding.UTF8,
                            false,
                            true))//<- this boolean says that we sant indentation
                {
                    DatabasejsonSerializer.WriteObject(writer, UserList);
                    DatabasejsonSerializer.WriteObject(writer, AddedThemeFromUser);
                }
            }
        }

        public Database LoadDatabaseData()
        {
            if (File.Exists(DatabaseDataFilePath))
            {
                using (FileStream fileStream = File.OpenRead(DatabaseDataFilePath))
                {
                    Database? database = (Database?)DatabasejsonSerializer.ReadObject(fileStream);
                    if (database == null)
                    {
                        throw new FileException("Failed to load database. The loaded object is null.");
                    }
                    else
                    {
                        return database;
                    }
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }

        public List<Theme> LoadDefaultTheme()
        {
            if (File.Exists(DefaultThemePath))
            {
                using (FileStream fileStream = File.OpenRead(DefaultThemePath))
                {
                    List<Theme>? DefaultThemeList = (List<Theme>?)DatabasejsonSerializer.ReadObject(fileStream);
                    if (DefaultThemeList == null)
                    {
                        throw new FileException("Failed to Default Theme. The loaded object is null.");
                    }
                    else
                    {
                        return DefaultThemeList;
                    }
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }
        public List<Logo> LoadDefaultLogo()
        {
            if (File.Exists(DefaultLogoPath))
            {
                using (FileStream fileStream = File.OpenRead(DefaultLogoPath))
                {
                    List<Logo>? DefaultLogoList = (List<Logo>?)DatabasejsonSerializer.ReadObject(fileStream);
                    if (DefaultLogoList == null)
                    {
                        throw new FileException("Failed to Default Logo. The loaded object is null.");
                    }
                    else
                    {
                        return DefaultLogoList;
                    }
                }
            }
            else
            {
                throw new FileException("No data file found.");
            }
        }
    }
}
