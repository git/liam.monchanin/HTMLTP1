﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    [DataContract(IsReference = true)]
    public class Note
    {
        [DataMember]
        private string name;
        [DataMember]
        public string Name
        {
            get { return name; }
            private set { if (value == null) { name = "Unnamed Note"; } else { name = value; } }
        }

        [DataMember]
        private string Text { get; set; } = "";

        [DataMember]
        private string logoPath;
        [DataMember]
        public string LogoPath
        {
            get { return logoPath; }
            private set { if (value == null) { logoPath = "PATH TO DEFAULT LOGO"; } else { logoPath = value; } }
        }

        [DataMember]
        private DateOnly CreationDate { get; }
        [DataMember]
        private DateOnly ModificationDate { get; set; }
        [DataMember]
        private readonly List<NoteImage> ImageList;
        [DataMember]
        private readonly List<User> Collaborators;
        [DataMember]
        private readonly List<User> Editors;
        [DataMember]
        private readonly User Owner;

        public Note(string name, string logoPath, User owner)
        {
            Name = name;
            LogoPath = logoPath;
            CreationDate = DateOnly.FromDateTime(DateTime.Now);
            ModificationDate = DateOnly.FromDateTime(DateTime.Now);
            ImageList = new List<NoteImage>();
            Collaborators = new List<User>();
            Editors = new List<User>();
            Owner = owner;
        }

        public string GetName() { return Name; }
        public string GetLogoPath() { return LogoPath; }
        public string GetText() { return Text; }
        public DateOnly GetCreationDate() { return CreationDate; }
        public DateOnly GetModificationDate() { return ModificationDate; }
        public List<NoteImage> GetImageList() { return ImageList; }
        public List<User> GetCollaborators() { return Collaborators; }
        public List<User> GetEditors() { return Editors; }
        public User GetOwner() { return Owner; }

        public override string ToString() => $"Note -> Name: {Name}\nLogoPath: {LogoPath}";

        public void SetName(string name) { Name = name; }
        public void SetLogoPath(string logoPath) { LogoPath = logoPath; }
        public void SetModificationDate() { ModificationDate = DateOnly.FromDateTime(DateTime.Now); }

        /// <summary>
        /// vérifier si l'utilisateur est le propriétaire de la note
        /// </summary>
        public bool VerifyOwner(User user) 
        { 
            return user == Owner;
        }

        public void AddImage(string imageLink, string position)
        {
            int newname = ImageList.Count + 1;
            ImageList.Add(new NoteImage(newname, imageLink, position));
        }

        public void RemoveImage(int name)
        {
            foreach (NoteImage image in ImageList)
            {
                if (image.GetName() == name)
                {
                    ImageList.Remove(image);
                    return;
                }
            }
            throw new NotFoundException("Image not found");
        }

        public void AddText(string text)
        {
            Text = Text + "\n" + text;
        }

        /// <summary>
        /// vérifier si l'utilisateur est un éditeur de la note
        /// </summary>
        public bool VerifyPrivilege(User user)
        {
            return Editors.Contains(user);
        }

        /// <summary>
        /// ajouter un utilisateur en tant que coopérateur
        /// </summary>
        public void AddCollaborator(User owner, User user)
        {
            if (VerifyOwner(owner)) { Collaborators.Add(user); }
            else { throw new NotAllowedException("User is not the owner"); }
            user.GetNoteList().Add(this);
        }

        /// <summary>
        /// supprimer un utilisateur en tant que coopérateur
        /// </summary>
        public void RemoveCollaborator(User owner, User user)
        {
            if (VerifyOwner(owner)) { Collaborators.Remove(user); }
            else { throw new NotAllowedException("User is not the owner"); }
            user.GetNoteList().Remove(this);
        }

        /// <summary>
        /// passer un coopérateur en tant qu'éditeur
        /// </summary>
        public void AddEditor(User owner, User user)
        {
            if (VerifyOwner(owner)) { Editors.Add(user); }
            else { throw new NotAllowedException("User is not the owner"); }
        }

        /// <summary>
        /// supprimer un coopérateur de la liste des éditeurs
        /// </summary>
        public void RemoveEditor(User owner, User user)
        {
            if (VerifyOwner(owner)) { Editors.Remove(user); }
            else { throw new NotAllowedException("User is not the owner"); }
        }
    }
}
