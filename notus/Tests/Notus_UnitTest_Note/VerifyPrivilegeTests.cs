﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class VerifyPrivilegeTests
    {
        [Test]
        public void VerifyPrivilege_WhenUserIsEditor_ReturnsTrue()
        {
            User editor = new User("Editor1", "editor1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", new User("Owner", "owner@example.com", "password"));
            note.AddEditor(note.GetOwner(), editor);
            bool result = note.VerifyPrivilege(editor);
            Assert.IsTrue(result);
        }

        [Test]
        public void VerifyPrivilege_WhenUserIsNotEditor_ThrowsException()
        {
            User editor = new User("Editor1", "editor1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", new User("Owner", "owner@example.com", "password"));
            bool result = note.VerifyPrivilege(editor);
            Assert.IsFalse(result);
        }
    }
}
