﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Database
{
    public class ModifyThemeColorListTests
    {
        private Database database;

        [SetUp]
        public void Setup()
        {
            database = new Database();
        }

        [Test]
        public void ModifyThemeColorList_ExistingTheme_ModifiesColors()
        {
            List<string> listcolor = new List<string>() { "Blue", "Dark", "Grey" };
            Theme theme = new Theme("ocean", listcolor);
            database.GetThemeList().Add(theme);
            List<string> newColorList = new List<string> { "Red", "Green", "Blue" };
            database.ModifyThemeColorList(theme, newColorList);
            Assert.That(theme.GetColor(0), Is.EqualTo(newColorList[0]));
            Assert.That(theme.GetColor(1), Is.EqualTo(newColorList[1]));
            Assert.That(theme.GetColor(2), Is.EqualTo(newColorList[2]));
        }
    }
}
