﻿using Biblioteque_de_Class;

namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class AddEditorTests
    {
        [Test]
        public void AddEditor_WhenUserIsOwner_EditorAdded()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User editor = new User("Editor1", "editor1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            note.AddEditor(owner, editor);
            Assert.IsTrue(note.GetEditors().Contains(editor));
        }

        [Test]
        public void AddEditor_WhenUserIsNotOwner_ThrowsException()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User editor = new User("Editor1", "editor1@example.com", "password");
            User user = new User("User1", "user1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            Assert.Throws<NotAllowedException>(() => note.AddEditor(user, editor));
        }
        
    }
}
