﻿using Microsoft.VisualBasic.FileIO;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Biblioteque_de_Class
{
    [DataContract(IsReference = true)]
    public class User
    {
        [DataMember]
        private string Username { get; set; }
        [DataMember]
        private string Email { get; set; }
        [DataMember]
        private string Password { get; set; }
        [DataMember]
        private string Picture { get; set; }
        [DataMember]
        private Theme Theme;
        [DataMember]
        private List<Note> NoteList;
        [DataMember]
        private List<Tags> TagList;
        [DataMember]
        private List<Note> FavList;
        [DataMember(EmitDefaultValue = false)]
        private bool IsConnected { get; set; }
        [DataMember]
        private Dictionary<Note, List<Tags>> NoteTagged;

        public User(string username, string email, string password)
        {
            Username = username;
            Email = email;
            Password = password;
            Picture = "defaultpicture.png";
            NoteList = new List<Note>();
            TagList = new List<Tags>();
            FavList = new List<Note>();
            NoteTagged = new Dictionary<Note, List<Tags>>();
        }

        public string GetUsername() { return Username; }
        public string GetEmail() { return Email; }
        public string GetPassword() { return Password; }
        public string GetPicture() { return Picture;}
        public Theme GetTheme() { return Theme; }
        public List<Note> GetNoteList() { return NoteList; }
        public List<Tags> GetTagList() { return TagList; }
        public List<Note> GetFavList() { return FavList; }
        public bool GetIsConnected() { return IsConnected; }
        public Dictionary<Note, List<Tags>> GetNoteTagged() { return NoteTagged; }
        public List<Tags> GetNoteTaggedList(Note note) { return NoteTagged[note]; }

        public void SetUsername(string username) { Username = username; }
        public void SetEmail(string email) { Email = email; }
        public void SetPassword(string password) { Password = password; }
        public void SetPicture(string picture) {  Picture = picture; }
        public void SetTheme(Theme theme) { Theme = theme; }
        public void SetIsConnected(bool isConnected) { IsConnected = isConnected; }

        public override string ToString() => $"username: {Username}\nemail: {Email}\npassword: {Password}\nOwned notes: {NoteList.Count}";

        /// <summary>
        /// rechercher une note dans la liste de note de l'utilisateur et la liste de note favoris de l'utilisateur
        /// </summary>
        public List<Note> SearchNoteByName(List<Note> ToResearchIntoList, string name)
        {
            List<Note> searchedNotes = new List<Note>();
            string search = name.ToLower();
            foreach (Note note in ToResearchIntoList)
            {
                if (note.GetName().ToLower().Contains(search))
                {
                    searchedNotes.Add(note);
                }
            }
            return searchedNotes;
        }

        /// <summary>
        /// rechercher un tag dans la liste de tag de l'utilisateur
        /// </summary>
        public List<Tags> SearchTagByName(List<Tags> ToResearchIntoList, string name)
        {
            List<Tags> searchedTags = new List<Tags>();
            string search = name.ToLower();
            foreach (Tags tag in ToResearchIntoList)
            {
                if (tag.GetName().ToLower().Contains(search))
                {
                    searchedTags.Add(tag);
                }
            }
            return searchedTags;
        }

        public List<Note> SearchNoteByDate(List<Note> ToResearchIntoList, string name, DateOnly FirstDate, DateOnly SecondeDate)
        {
            List<Note> searchedNotes = new List<Note>();
            string search = name.ToLower();
            foreach (Note note in ToResearchIntoList)
            {
                if (note.GetName().ToLower().Contains(search) && note.GetCreationDate() >= FirstDate && note.GetCreationDate() <= SecondeDate || note.GetModificationDate() >= FirstDate && note.GetModificationDate() <= SecondeDate)
                {
                    searchedNotes.Add(note);
                }
            }
            return searchedNotes;
        }

        public Note GetNoteByName(string name)
        {
            foreach (Note note in NoteList)
            {
                if (note.GetName() == name)
                {
                    return note;
                }
            }
            throw new NotFoundException("Note not found");
        }

        public Tags GetTagByName(string name)
        {
            foreach (Tags tag in TagList)
            {
                if (tag.GetName() == name)
                {
                    return tag;
                }
            }
            throw new NotFoundException("Tag not found");
        }

        /// <summary>
        /// ajouter une note dans la liste de note favorite de l'utilisateur
        /// </summary>
        public void AddFavorite(Note note)
        {
            if (FavList.Contains(note))
            {
                throw new AlreadyExistException("Note already in favorites");
            }
            FavList.Add(note);
        }

        /// <summary>
        /// supprimer une note dans la liste de note favorite de l'utilisateur
        /// </summary>
        public void RemoveFavorite(Note note)
        {
            if (FavList.Contains(note))
            {
                FavList.Remove(note);
            }
            else
            {
                throw new NotFoundException("Note not found");
            }
        }

        /// <summary>
        ///creer une note
        /// </summary>
        public void CreateNote(string name, string imagePath)
        {
            foreach (Note existingNote in NoteList)
            {
                if (existingNote.GetName() == name)
                {
                    throw new AlreadyExistException("Note already exists");
                }
            }
            Note note = new Note(name, imagePath, this);
            NoteList.Add(note);
            NoteTagged.Add(note, new List<Tags>());
        }

        /// <summary>
        /// supprimer une note
        /// </summary>
        public void DeleteNote(string note)
        {
            foreach (Note existingNote in NoteList)
            {
                if (existingNote.GetName() == note)
                {
                    NoteList.Remove(existingNote);
                    NoteTagged.Remove(existingNote);
                    return;
                }
            }
            throw new NotFoundException("Note not found");
        }

        /// <summary>
        /// creer un tag
        /// </summary>
        public void CreateTag(string name, string color)
        {
            foreach (Tags tag in TagList)
            {
                if (tag.GetName() == name)
                {
                    throw new AlreadyExistException("Tag already exists");
                }
            }
            TagList.Add(new Tags(name, color));
        }

        /// <summary>
        /// supprimer un tag
        /// </summary>
        public void DeleteTag(string name)
        {
            foreach (Tags tag in TagList)
            {
                if (tag.GetName() == name)
                {
                    TagList.Remove(tag);
                    return;
                }
            }
        }

        /// <summary>
        /// ajouter un tag a une note
        /// </summary>
        public void AddTagToNoteList(Note note, Tags tagToAdd)
        {
            if (!TagList.Contains(tagToAdd))
            {
                throw new NotFoundException("Tag not found");
            }
            if (!NoteList.Contains(note))
            {
                throw new NotFoundException("Note not found");
            }
            NoteTagged[note].Add(tagToAdd);
        }

        /// <summary>
        /// supprimer un tag a une note
        /// </summary>
        public void RemoveTagFromNoteList(Note note, Tags tagToRemove)
        {
            if (!TagList.Contains(tagToRemove))
            {
                throw new NotFoundException("Tag not found");
            }
            if (!NoteList.Contains(note))
            {
                throw new NotFoundException("Note not found");
            }
            NoteTagged[note].Remove(tagToRemove);
        }

        /// <summary>
        /// ajouter plusieur tag a une note
        /// </summary>
        public void AddTagsToNoteList(Note note, List<Tags> tags)
        {
            NoteTagged.Add(note, tags);
        }

        /// <summary>
        ///supprimer tout les tags d'une note
        /// </summary>
        public void RemoveTagsFromNoteList(Note note)
        {
            NoteTagged.Remove(note);
        }

    }
}