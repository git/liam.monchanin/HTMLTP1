﻿using Biblioteque_de_Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notus_UnitTest_Note
{
    [TestFixture]
    public class RemoveEditorTests
    {
        [Test]
        public void RemoveEditor_WhenUserIsOwner_EditorRemoved()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User editor = new User("Editor1", "editor1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            note.AddEditor(owner, editor);
            note.RemoveEditor(owner, editor);
            Assert.IsFalse(note.GetEditors().Contains(editor));
        }

        [Test]
        public void RemoveEditor_WhenUserIsNotOwner_ThrowsException()
        {
            User owner = new User("Owner", "owner@example.com", "password");
            User editor = new User("Editor1", "editor1@example.com", "password");
            User user = new User("User1", "user1@example.com", "password");
            Note note = new Note("Test Note", "logo.png", owner);
            note.AddEditor(owner, editor);
            Assert.Throws<NotAllowedException>(() => note.RemoveEditor(user, editor));
        }
        
    }
}
