﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    public class Theme
    {
        private string Name { get; set; }
        private readonly List<string> ColorList;

        public Theme(string name, List<string> colorList)
        {
            Name = name;
            ColorList = colorList;
        }

        public string GetName() { return Name; }
        public void SetName(string name) { Name = name; }
        public List<string> GetColorList() { return ColorList; }
        public string GetColor(int index) { return ColorList[index]; }

        public override string ToString() => $"name: {Name}\ncolor 1: {ColorList[0]}\ncolor 2: {ColorList[1]}\ncolor 3: {ColorList[2]}\n";

        /// <summary>
        /// Change a specific color of the theme.
        /// </summary>
        public void ChangeColor(string color, string newColor)
        {
            for (int i = 0; i < ColorList.Count; i++)
            {
                if (ColorList[i] == color)
                {
                    ColorList[i] = newColor;
                }
            }
        }
    }
}
