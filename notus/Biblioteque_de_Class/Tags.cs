﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteque_de_Class
{
    public class Tags
    {
        private string Name { get; set; }
        private string Color { get; set; }
            
        public Tags(string name, string color)
        {
            Name = name;
            Color = color;
        }

        public string GetName() { return Name; }
        public string GetColor() { return Color; }
        public void SetName(string name) { Name = name; }
        public void SetColor(string color) { Color = color; }

        public override string ToString() => $"tag -> name: {Name}\ncolor: {Color}";
    }
}
